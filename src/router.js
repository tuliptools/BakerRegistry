import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Address from "./views/Address";

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/:address',
      name: 'address',
      component: Address
    },
  ]
})
